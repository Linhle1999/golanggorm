package handler

import (
	"day5/user/model"
	"fmt"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

func (h *UserHandler) LoginMongo(ctx echo.Context) error {
	username := ctx.FormValue("username")
	pws := ctx.FormValue("password")

	token, err := h.userRepo.Login(username, pws)

	if err != nil {
		return ctx.JSON(http.StatusNotAcceptable, err.Error())
	}

	return ctx.JSON(http.StatusOK, token)
}

func (h *UserHandler) SignUpMongo(ctx echo.Context) error {
	username := ctx.FormValue("username")
	pws := ctx.FormValue("password")

	user := model.User{
		Username: username,
	}

	user.AssignPassword(pws)
	_, err := h.userRepo.InsertUser(user)

	if err != nil {
		return err
	}
	return ctx.JSON(http.StatusOK, "Đã thêm")
}

func (h *UserHandler) UpdateUserMongo(ctx echo.Context) error {
	user, _ := h.IsLogin(ctx)

	pws := ctx.FormValue("password")

	user.AssignPassword(pws)

	result, _ := h.userRepo.UpdateUser(user)

	return ctx.JSON(http.StatusOK, result)
}

func (h *UserHandler) DeleteUserMongo(ctx echo.Context) error {
	id := ctx.Param("id")

	result, _ := h.userRepo.DeleteUser(id)

	return ctx.JSON(http.StatusOK, result)
}

func (h *UserHandler) GetAllMongo(ctx echo.Context) error {
	result, _ := h.userRepo.GetUsers()
	fmt.Println(result)
	return ctx.JSON(http.StatusOK, result)
}

func (h *UserHandler) GetCartByUserMongo(ctx echo.Context) error {
	user, _ := h.IsLogin(ctx)

	result, err := h.cartRepo.GetCartsOfUser(user.ID)

	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, result)
}

func (h *UserHandler) GetCardItemsByUserMongo(ctx echo.Context) error {
	user, _ := h.IsLogin(ctx)

	// userid, _ := strconv.Atoi(ctx.Param("userid"))

	results, _ := h.cartRepo.GetCartItemsOfUser(user.ID)

	return ctx.JSON(http.StatusOK, results)
}

func (h *UserHandler) RemoveCartMongo(ctx echo.Context) error {
	id := ctx.Param("id")

	err := h.cartRepo.RemoveCart(id)

	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, "Remove Success Cart!")
}

func (h *UserHandler) RemoveCartItemMongo(ctx echo.Context) error {
	err := h.cartRepo.RemoveCartItem(ctx.Param("id"))

	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, "Remove Success Cartitem!")
}

func (h *UserHandler) UpdateCartItemMongo(ctx echo.Context) error {
	name := ctx.FormValue("name")
	price, _ := strconv.Atoi(ctx.FormValue("price"))

	cartitem := model.CartItem{
		ID:    ctx.Param("id"),
		Name:  name,
		Price: price,
	}

	err := h.cartRepo.UpdateCartItem(cartitem)

	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, "Update Success Cartitem!")
}

func (h *UserHandler) DeleteCartMongo(ctx echo.Context) error {
	err := h.cartRepo.RemoveCart(ctx.Param("id"))
	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, "Delete cart success!")
}
