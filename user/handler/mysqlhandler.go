package handler

import (
	"day5/user/model"
	"day5/user/repository"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

type UserHandler struct {
	// interface
	userRepo repository.UserRepo
	cartRepo repository.CartRepo
}

func NewBaseHandler(userRepo repository.UserRepo, cartRepo repository.CartRepo) *UserHandler {
	return &UserHandler{
		userRepo: userRepo,
		cartRepo: cartRepo,
	}
}

func (h *UserHandler) Login(ctx echo.Context) error {
	username := ctx.FormValue("username")
	pws := ctx.FormValue("password")

	token, err := h.userRepo.Login(username, pws)

	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, token)
}

func (h *UserHandler) SignUp(ctx echo.Context) error {
	username := ctx.FormValue("username")
	pws := ctx.FormValue("password")

	user := model.User{
		Username: username,
	}

	user.AssignPassword(pws)

	fmt.Println(username)
	result, _ := h.userRepo.InsertUser(user)

	return ctx.JSON(http.StatusOK, result)
}

func (h *UserHandler) InsertUser(ctx echo.Context) error {
	username := ctx.FormValue("username")
	pws := ctx.FormValue("password")
	// fullname := ctx.FormValue("fullname")

	user := model.User{
		Username: username,
		Password: pws,
		// Fullname: fullname,
	}
	result, _ := h.userRepo.InsertUser(user)

	return ctx.JSON(http.StatusOK, result)
}

func (h *UserHandler) UpdateUser(ctx echo.Context) error {
	id := ctx.Param("id")
	username := ctx.FormValue("username")
	pws := ctx.FormValue("password")
	// fullname := ctx.FormValue("fullname")

	user := model.User{
		ID:       id,
		Username: username,
		Password: pws,
		// Fullname: fullname,
	}

	result, _ := h.userRepo.UpdateUser(user)

	return ctx.JSON(http.StatusOK, result)
}

func (h *UserHandler) DeleteUser(ctx echo.Context) error {
	id := ctx.Param("id")

	result, _ := h.userRepo.DeleteUser(id)

	return ctx.JSON(http.StatusOK, result)
}

func (h *UserHandler) GetAll(ctx echo.Context) error {
	result, _ := h.userRepo.GetUsers()

	return ctx.JSON(http.StatusOK, result)
}

func (h *UserHandler) GetCardItems(ctx echo.Context) error {
	user, _ := h.IsLogin(ctx)

	// userid, _ := strconv.Atoi(ctx.Param("userid"))

	results, _ := h.cartRepo.GetCartItemsOfUser(user.ID)

	return ctx.JSON(http.StatusOK, results)
}

func (h *UserHandler) RemoveCart(ctx echo.Context) error {
	err := h.cartRepo.RemoveCart(ctx.Param("id"))

	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, "Remove Success Cart!")
}

func (h *UserHandler) RemoveCartItem(ctx echo.Context) error {
	err := h.cartRepo.RemoveCartItem(ctx.Param("id"))

	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, "Remove Success Cartitem!")
}

func (h *UserHandler) UpdateCartItem(ctx echo.Context) error {
	name := ctx.FormValue("name")
	price, _ := strconv.Atoi(ctx.FormValue("price"))

	cartitem := model.CartItem{
		ID:    ctx.Param("id"),
		Name:  name,
		Price: price,
	}
	err := h.cartRepo.UpdateCartItem(cartitem)

	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, "Update Success Cartitem!")
}

func (h *UserHandler) DeleteCart(ctx echo.Context) error {
	user, _ := h.IsLogin(ctx)

	// userid, _ := strconv.Atoi(ctx.Param("userid"))

	results, _ := h.cartRepo.GetCartItemsOfUser(user.ID)

	return ctx.JSON(http.StatusOK, results)
}

func (h *UserHandler) GetCart(ctx echo.Context) error {
	user, _ := h.IsLogin(ctx)

	// userid, _ := strconv.Atoi(ctx.Param("userid"))

	results, _ := h.userRepo.GetUserByUsername(user.Username)

	return ctx.JSON(http.StatusOK, results)
}

func (h *UserHandler) GetCartItem(ctx echo.Context) error {
	user, _ := h.IsLogin(ctx)

	// userid, _ := strconv.Atoi(ctx.Param("userid"))

	results, _ := h.cartRepo.GetCartItemsOfUser(user.ID)

	return ctx.JSON(http.StatusOK, results)
}

func (h *UserHandler) InitCart(ctx echo.Context) error {
	h.cartRepo.InitCart()

	return ctx.JSON(http.StatusOK, "")
}

func (h *UserHandler) IsLogin(ctx echo.Context) (model.User, error) {
	user, ok := ctx.Get("user").(*jwt.Token) //user only available if authentication middleware was used
	if !ok {
		return model.User{}, errors.New("not exits user in store")
	}

	claims := user.Claims.(jwt.MapClaims)
	username := claims["name"].(string)

	value, _ := h.userRepo.GetUserByUsername(username)

	return value, nil
}
