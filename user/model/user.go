package model

import "golang.org/x/crypto/bcrypt"

type User struct {
	ID       string `gorm:"primaryKey,autoIncrement" json:"_id,omitempty" bson:"_id,omitempty"`
	Username string
	Password string
	// Fullname string
}

func (u *User) AssignPassword(passw string) error {
	hashed, err := bcrypt.GenerateFromPassword([]byte(passw), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	u.Password = string(hashed)
	return nil
}
