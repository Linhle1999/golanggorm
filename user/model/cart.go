package model

import (
	"time"
)

type Cart struct {
	ID        string `gorm:"primaryKey,autoIncrement" json:"_id,omitempty" bson:"_id,omitempty"`
	CreatedAt time.Time

	UserID string

	CartItems []CartItem
}

type CartItem struct {
	ID    string `gorm:"primaryKey,autoIncrement" json:"_id,omitempty" bson:"_id,omitempty"`
	Name  string
	Price int

	CartID string  `gorm:"size:255;uniqueIndex"`
}
