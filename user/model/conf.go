package model

type Configuration struct {
	MySQL struct {
		User     string `envconfig:"MySQL_USER" default:"root"`
		Password string `envconfig:"MySQL_PASSWORD" default:"0000"`
		Port     string `envconfig:"MySQL_PORT" default:"3306"`
		Host     string `envconfig:"MySQL_HOST" default:"localhost"`
		Database string `envconfig:"MySQL_DATABASE" default:"golang"`
	}

	Mongo struct {
		Host string `envconfig:"MONGO_HOST" default:"localhost"`
		Port string `envconfig:"MONGO_PORT" default:"27017"`
	}
}
