package db

import (
	"day5/user/model"
	"errors"
	"fmt"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

func (gorm *GormDB) Login(username, password string) (string, error) {
	db := gorm.db

	user := model.User{}

	db.Table("users").Find(&user, "username = ?", username)

	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil {
		return "", errors.New("please provide valid login details")
	}
	fmt.Println(err)

	// Create token
	token := jwt.New(jwt.SigningMethodHS256)
	fmt.Println(token)

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	//TODO: set real username
	claims["name"] = user.Username
	claims["admin"] = true
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte("secret"))
	if err != nil {
		fmt.Println(err)

		return "", err
	}

	fmt.Println(t)

	if user.Username == "" {
		return "", errors.New("Incorrect")
	} else {
		return t, nil
	}
}

func (gorm *GormDB) GetUserByUsername(username string) (model.User, error) {
	db := gorm.db

	user := model.User{}
	err := db.Table("users").Where("username LIKE ?", username).Find(&user).Error

	if err != nil {
		return model.User{}, err
	}
	return user, nil
}

func (gorm *GormDB) InsertUser(user model.User) (model.User, error) {
	db := gorm.db

	user.AssignPassword(user.Password)

	fmt.Println(user)

	user.ID = uuid.NewString()
	err := db.Create(&user).Error

	if err != nil {
		return model.User{}, err
	}
	return user, nil
}

func (gorm *GormDB) UpdateUser(user model.User) (model.User, error) {
	db := gorm.db

	user.AssignPassword(user.Password)

	err := db.Table("users").Where("id = ?", user.ID).Updates(map[string]interface{}{"username": user.Username, "password": user.Password}).Error

	if err != nil {
		return model.User{}, err
	}
	return user, nil
}

func (gorm *GormDB) DeleteUser(userID string) (bool, error) {
	db := gorm.db

	// user.AssignPassword(user.Password)

	user := model.User{}
	err := db.Table("users").Where("id = ?", userID).Delete(&user).Error

	if err != nil {
		return false, err
	}
	return true, nil
}

func (gorm *GormDB) GetUsers() ([]model.User, error) {
	db := gorm.db

	result := []model.User{}

	err := db.Table("users").Find(&result).Error

	if err != nil {
		return result, err
	}
	return result, nil
}

func CreateToken(userid int) (string, error) {
	var err error
	//Creating Access Token
	os.Setenv("ACCESS_SECRET", "test2021") //this should be in an env file
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["user_id"] = userid
	atClaims["exp"] = time.Now().Add(time.Minute * 15).Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString([]byte(os.Getenv("ACCESS_SECRET")))
	if err != nil {
		return "", err
	}
	return token, nil
}
