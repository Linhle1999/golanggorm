package db

import (
	"day5/user/model"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

const (
	DBUser     = "root"
	DBPass     = "0000"
	DBHost     = "localhost"
	DBDatabase = "golang"
	DBPort     = "3306"
)

type GormDB struct {
	db *gorm.DB
}

func NewUserRepo(conf model.Configuration) *GormDB {
	uri := conf.MySQL.User + ":" + conf.MySQL.Password + "@tcp" + "(" + conf.MySQL.Host + ":" + conf.MySQL.Port + ")/" + conf.MySQL.Database + "?" + "parseTime=true&loc=Local"
	return &GormDB{
		db: Connection(uri),
	}
}

func Connection(uri string) *gorm.DB {
	db, err := gorm.Open(mysql.Open(uri), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	db.AutoMigrate(&model.User{}, &model.Cart{}, &model.CartItem{})

	return db
}
