package db

import (
	"day5/user/model"
	"fmt"
	"math/rand"
	"time"

	"github.com/google/uuid"
)

func (gorm *GormDB) FindCartByUserID(userID string) (model.Cart, error) {
	db := gorm.db

	cart := model.Cart{}
	db.Table("carts").Find(&cart, "userid LIKE ?", userID)

	return cart, nil
}

func (gorm *GormDB) RemoveCart(cartID string) error {
	db := gorm.db

	err := db.Delete(model.Cart{}, "id LIKE ?", cartID).Error
	if err != nil {
		return err
	}
	return nil
}

func (gorm *GormDB) UpdateCarts(cart model.Cart) error {
	// db := gorm.db

	// err := db.Table("cart").Where("id = ?", cart.ID).Update(map[string]interface{}{}).Error
	// if err != nil {
	// 	return err
	// }

	return nil
}

func (gorm *GormDB) UpdateCartItem(cartItem model.CartItem) error {
	db := gorm.db

	err := db.Table("cart_items").Where("id LIKE ?", cartItem.ID).Updates(map[string]interface{}{"name": cartItem.Name, "price": cartItem.Price}).Error
	if err != nil {
		return err
	}

	return nil
}

func (gorm *GormDB) RemoveCartItem(cartItemID string) error {
	db := gorm.db

	err := db.Delete(model.CartItem{}, "id LIKE ?", cartItemID).Error
	if err != nil {
		return err
	}
	return nil
}

func (gorm *GormDB) GetCartItemsOfUser(userID string) ([]model.CartItem, error) {
	db := gorm.db

	var results []model.CartItem

	err := db.Joins("JOIN carts ON carts.id = cart_items.cart_id").Joins("JOIN users ON users.id = carts.user_id").Where("users.id LIKE ?", userID).Find(&results).Error

	if err != nil {
		return nil, err
	}
	return results, nil
}

func (gorm *GormDB) GetCartsOfUser(userID string) ([]model.Cart, error) {
	db := gorm.db

	carts := []model.Cart{}

	db.Joins("JOIN users ON users.id = carts.user_id").Where("users.id LIKE ?", userID).Find(&carts)

	return carts, nil
}

func (gorm *GormDB) InitCart() error {

	cartitems := []model.CartItem{}

	cartID := uuid.NewString()
	for i := 0; i < 5; i++ {
		cartitems = append(cartitems, model.CartItem{
			ID:     uuid.NewString(),
			Name:   RandStringRunes(10),
			Price:  rand.Intn(100),
			CartID: cartID,
		})
	}

	fmt.Println(cartitems)

	cart := model.Cart{
		ID:        cartID,
		CreatedAt: time.Now(),
		UserID:    "606a6e9edf3509116427f308",
		CartItems: cartitems,
	}

	gorm.db.Create(&cart)
	return nil
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
