package repository

import "day5/user/model"

type CartRepo interface {
	RemoveCart(string) error
	UpdateCarts(model.Cart) error
	UpdateCartItem(model.CartItem) error
	RemoveCartItem(string) error
	GetCartItemsOfUser(userID string) ([]model.CartItem, error)
	GetCartsOfUser(userID string) ([]model.Cart, error)

	InitCart() error
}
