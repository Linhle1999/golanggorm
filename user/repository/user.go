package repository

import (
	"day5/user/model"
)

type UserRepo interface {
	Login(username, password string) (string, error)
	GetUserByUsername(username string) (model.User, error)
	InsertUser(user model.User) (model.User, error)
	UpdateUser(user model.User) (model.User, error)
	DeleteUser(string) (bool, error)
	GetUsers() ([]model.User, error)
}
