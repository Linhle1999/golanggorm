package mongodb

import (
	"context"
	"day5/user/model"
	"errors"
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"golang.org/x/crypto/bcrypt"
)

func (mongoDB *MongoDB) Login(username, password string) (string, error) {
	collection := mongoDB.client.Database("mongo").Collection("user")

	user := model.User{}

	filter := bson.M{"username": username}

	collection.FindOne(context.TODO(), filter).Decode(&user)

	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil {
		return "", errors.New("please provide valid login details")
	}

	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	//TODO: set real username
	claims["name"] = user.Username
	claims["admin"] = true
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte("secret"))
	if err != nil {
		return "", err
	}

	if user.Username == "" {
		return "", errors.New("Incorrect")
	} else {
		return t, nil
	}
}

func (mongoDB *MongoDB) GetUserByUsername(username string) (model.User, error) {
	// filter := model.User{
	// 	Username: username,
	// }
	filter := bson.M{"username": username}

	collection := mongoDB.client.Database("mongo").Collection("user")

	user := model.User{}
	err := collection.FindOne(context.TODO(), filter).Decode(&user)
	if err != nil {
		return model.User{}, err
	}
	// db := mongoDB.db
	// dulie := db.Find()

	return user, nil
}

func (mongoDB *MongoDB) InsertUser(user model.User) (model.User, error) {
	collection := mongoDB.client.Database("mongo").Collection("user")

	user.ID = uuid.NewString()
	collection.InsertOne(context.TODO(), user)
	return user, nil
}

func (mongoDB *MongoDB) UpdateUser(user model.User) (model.User, error) {
	filter := bson.M{"_id": user.ID}

	collection := mongoDB.client.Database("mongo").Collection("user")

	update := bson.M{"$set": bson.M{"username": user.Username, "password": user.Password}}

	result := model.User{}

	collection.FindOneAndUpdate(context.TODO(), filter, update).Decode(&result)

	fmt.Print(result)

	return user, nil
}

func (mongoDB *MongoDB) DeleteUser(id string) (bool, error) {
	collection := mongoDB.client.Database("mongo").Collection("user")

	filter := bson.M{"_id": id}
	_, err := collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (mongoDB *MongoDB) GetUsers() ([]model.User, error) {
	var users []model.User
	collection := mongoDB.client.Database("mongo").Collection("user")

	cursor, err := collection.Aggregate(context.TODO(), bson.D{})
	if err != nil {
		return nil, err
	}
	for cursor.Next(context.TODO()) {
		user := model.User{}
		err := cursor.Decode(&user)
		if err != nil {
			return nil, err
		}

		users = append(users, user)
	}
	return users, nil
}
