package mongodb

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"day5/user/model"
)

type MongoDB struct {
	client     *mongo.Client
}

func Connection(uri string) *mongo.Client {
	// Set client options
	clientOptions := options.Client().ApplyURI(uri)

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Connect success!")
		return client

	}

	return nil
}

func (mongoClient *MongoDB) Disconnect() error {
	err := mongoClient.client.Disconnect(context.TODO())

	if err != nil {
		return err
	}
	fmt.Println("Disconnect success!")
	return nil
}
func NewUserRepo(conf model.Configuration) *MongoDB {
	uri := string("mongodb://" + conf.Mongo.Host + ":" + conf.Mongo.Port)
	return &MongoDB{
		client: Connection(uri),
	}
}
