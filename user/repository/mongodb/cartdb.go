package mongodb

import (
	"context"
	"day5/user/model"
	"math/rand"
	"time"

	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
)

func (mongoDB *MongoDB) RemoveCart(cartID string) error {
	collection := mongoDB.client.Database("mongo").Collection("cart")

	filter := bson.M{"_id": cartID}

	err := collection.FindOneAndDelete(context.TODO(), filter).Err()

	if err != nil {
		return err
	}

	return nil
}

func (mongoDB *MongoDB) UpdateCarts(cart model.Cart) error {
	collection := mongoDB.client.Database("mongo").Collection("cart")

	filter := bson.M{"_id": cart.ID}

	err := collection.FindOneAndUpdate(context.TODO(), filter, cart).Err()

	if err != nil {
		return err
	}
	return nil
}

func (mongoDB *MongoDB) UpdateCartItem(cartItem model.CartItem) error {
	collection := mongoDB.client.Database("mongo").Collection("cart")

	cart := model.Cart{}
	err := collection.FindOne(context.TODO(), bson.M{"cartitems._id": cartItem.ID}).Decode(&cart)
	if err != nil {
		return err
	}

	filter := bson.M{"_id": cart.ID, "cartitems._id": cartItem.ID}
	update := bson.M{
		"$set": bson.M{
			"cartitems.$.name":  cartItem.Name,
			"cartitems.$.price": cartItem.Price},
	}

	err = collection.FindOneAndUpdate(context.TODO(), filter, update).Err()
	if err != nil {
		return err
	}

	return nil
}

func (mongoDB *MongoDB) RemoveCartItem(cartItemID string) error {
	collection := mongoDB.client.Database("mongo").Collection("cart")

	cart := model.Cart{}
	err := collection.FindOne(context.TODO(), bson.M{"cartitems._id": cartItemID}).Decode(&cart)
	if err != nil {
		return err
	}

	change := bson.M{"$pull": bson.M{"cartitems": bson.M{"_id": cartItemID}}}

	_, err = collection.UpdateOne(context.TODO(), bson.M{}, change)

	if err != nil {
		return err
	}
	return nil
}

func (mongoDB *MongoDB) GetCartItemsOfUser(userID string) ([]model.CartItem, error) {
	carts, _ := mongoDB.GetCartsOfUser(userID)

	cartItems := []model.CartItem{}

	for _, value := range carts {
		cartItems = append(cartItems, value.CartItems...)
	}

	return cartItems, nil
}

func (mongoDB *MongoDB) GetCartsOfUser(userID string) ([]model.Cart, error) {
	collection := mongoDB.client.Database("mongo").Collection("cart")

	carts := []model.Cart{}

	filter := bson.M{"userid": userID}

	cursor, err := collection.Find(context.TODO(), filter)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(context.TODO())
	for cursor.Next(context.TODO()) {
		data := model.Cart{}
		if err = cursor.Decode(&data); err != nil {
			return nil, err
		}
		carts = append(carts, data)
	}
	return carts, nil
}

func (mongoDB *MongoDB) InitCart() error {
	collection := mongoDB.client.Database("mongo").Collection("cart")

	cartitems := []model.CartItem{}

	cartID := uuid.NewString()

	for i := 0; i < 5; i++ {
		cartitems = append(cartitems, model.CartItem{
			ID:     uuid.NewString(),
			Name:   RandStringRunes(10),
			Price:  rand.Intn(100),
			CartID: cartID,
		})
	}

	cart := model.Cart{
		ID:        cartID,
		CreatedAt: time.Now(),
		UserID:    "606a6e9edf3509116427f308",
		CartItems: cartitems,
	}

	collection.InsertOne(context.TODO(), cart)
	return nil
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
