package main

import (
	"day5/user/handler"
	"day5/user/model"
	"day5/user/repository/mongodb"
	"fmt"

	"github.com/kelseyhightower/envconfig"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	// "github.com/joho/godotenv"
)

var conf model.Configuration

func init() {
	if err := envconfig.Process("", &conf); err != nil {
		fmt.Printf("err=%s\n", err)
	}

	fmt.Println(conf)
}

func main() {
	e := echo.New()

	// gorm := db.NewUserRepo(conf)
	mongodb := mongodb.NewUserRepo(conf)
	defer mongodb.Disconnect()

	mongocontroller := handler.NewBaseHandler(mongodb, mongodb)
	// mysqlcontroller := handler.NewBaseHandler(gorm, gorm)

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.POST("/mongo/login", mongocontroller.LoginMongo)
	e.POST("/mongo/signup", mongocontroller.SignUpMongo)

	// e.POST("/mysql/login", mysqlcontroller.Login)
	// e.POST("/mysql/signup", mysqlcontroller.SignUp)

	r := e.Group("")
	r.Use(middleware.JWT([]byte("secret")))

	//Mongo
	r.PUT("/mongo/updateone/:id", mongocontroller.UpdateUserMongo)

	r.GET("/mongo/list/user", mongocontroller.GetAllMongo)

	r.DELETE("/mongo/delete/:id", mongocontroller.DeleteUserMongo)

	r.GET("/mongo/list/cart", mongocontroller.GetCartByUserMongo)

	r.GET("/mongo/list/cartitem", mongocontroller.GetCardItemsByUserMongo)

	r.DELETE("/mongo/remove/cartitem/:id", mongocontroller.RemoveCartItemMongo)

	r.DELETE("/mongo/remove/cart/:id", mongocontroller.RemoveCartMongo)

	r.PUT("/mongo/update/cartitem/:id", mongocontroller.UpdateCartItemMongo)

	e.GET("/initcart", mongocontroller.InitCart)

	//MySQL

	// r.PUT("/mysql/updateone/:id", mysqlcontroller.UpdateUser)

	// r.GET("/mysql/list/user", mysqlcontroller.GetAll)

	// r.DELETE("/mysql/delete/:id", mysqlcontroller.DeleteUser)

	// r.GET("/mysql/list/cart", mysqlcontroller.GetCart)

	// r.GET("/mysql/list/cartitem", mysqlcontroller.GetCardItems)

	// r.DELETE("/mysql/remove/cartitem/:id", mysqlcontroller.RemoveCartItemMongo)

	// r.DELETE("/mysql/remove/cart/:id", mysqlcontroller.RemoveCartMongo)

	// r.PUT("/mysql/update/cartitem/:id", mysqlcontroller.UpdateCartItemMongo)

	// e.GET("/mysql/initcart", mysqlcontroller.InitCart)

	e.Logger.Fatal(e.Start(":5555"))
}
